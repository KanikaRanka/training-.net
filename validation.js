//Form validation using jQuery
    
    // Validate Firstname
    $(document).ready(function(){
    $("#fname").keyup(function(){
	var fName=$("#fname").val();
	if(fName.length==0)
	{
		$("#fnameError").show();
		$("#fnameError").html("Input field must required");
        $("#fnameError").focus();
		$("#fnameError").css("color","red");
	}
	 else if(fName.length>50)
	{
		$("#fnameError").show();
		$("#fnameError").html("Input field must be less than 50");
		$("#fnameError").css("color","red");
	}
    else if(!isNaN(fName))
    {
    $("#fnameError").show();
    $("#fnameError").html("Only Alphabets are allowed");
    $("#fnameError").focus();
    $("#fnameError").css("color","red");
    }
	else
	{
		$("#fnameError").hide();
	}
	});
    
    //Validate Last name
    $("#lname").keyup(function(){
	var lName=$("#lname").val();
	if(lName.length==0)
	{
		$("#lnameError").show();
		$("#lnameError").html("Input field must required");
		$("#lnameError").css("color","red");
	}
	 else if(lName.length>25)
	{
		$("#lnameError").show();
		$("#lnameError").html("Input field must be less than 25");
		$("#lnameError").css("color","red");
	}
    else if(!isNaN(lName))
    {
    $("#lnameError").show();
    $("#lnameError").html("Only Alphabets are allowed");
    $("#lnameError").focus();
    $("#lnameError").css("color","red");
    }
	else
	{
		$("#lnameError").hide();
	}
	});
	
    
	//Validate Phone No.
	$("#pNo").keyup(function(){
		var telephone=$("#pNo").val();
	if(telephone.length==0)
	{
		$("#phoneError").show();
		$("#phoneError").html("Input field must required");
		$("#phoneError").css("color","red");
	}
	else if(isNaN(telephone))
	{
		$("#phoneError").show();
		$("#phoneError").html("Please insert the numberic input");
		$("#phoneError").css("color","red");
	}
	else if(telephone.length!=10)
	{
		$("#phoneError").show();
		$("#phoneError").html("Phone number should be 10 digits");
		$("#phoneError").css("color","red");
	}
	else
	{
		$("#phoneError").hide();
	}
	});
    
   // Validate email
    $("#email").keyup(function(){
	var email=$("#email").val();
	 var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(email.length==0)
	{
		$("#emailError").show();
		$("#emailError").html("Input field must required");
		$("#emailError").css("color","red");	
	}
	else if(!regex.test(email))
	{
		$("#emailError").show();
		$("#emailError").html("Email must contain '@' and '.com'");
        $("#emailError").css("color","red");
	}
	else
	{
		$("#emailError").hide();
	}
    }); 
    
    // Validate Address
   
    $("#address").keyup(function(){
	var add=$("#address").val();
	if(add.length==0)
	{
		$("#addressError").show();
		$("#addressError").html("Input field must required");
        $("#addressError").focus();
		$("#addressError").css("color","red");
	}
	 else if(add.length>250)
	{
		$("#addressError").show();
		$("#addressError").html("Input field must required");
		$("#addressError").css("color","red");
	}
	else
	{
		$("#addressError").hide();
	}
	});

    
}); 

function validation()
	{
		var fName=document.getElementById("fname").value;
		var lName=document.getElementById("lname").value;
		var emailid=document.getElementById("email").value;
		var phoneNo=document.getElementById("pNo").value;
        var collegeName=document.getElementById("colname").value;
        var city=document.getElementById("city").value;
        var state=document.getElementById("state").value;
        var percent=document.getElementById("per").value;
		var address=document.getElementById("address").value;
        
        var status = true;

		if(fName == "")
		{
		document.getElementById('fnameError').innerHTML="First Name is required";
        document.getElementById("fnameError").style.color ="red";
        
		status = false;
		}
        
		else if(fName.length <=2 || fname.length>50)
		{
		document.getElementById('fnameError').innerHTML="Only 50 characters are allowed";
        document.getElementById("fnameError").style.color ="red";
		status = false;
		}
        
		else if(!isNaN(fName))
		{
		document.getElementById('fnameError').innerHTML="Only Alphabets are allowed";
        document.getElementById("fnameError").style.color ="red";
		status= false;
		}
        
		if(lName == "")
		{
		document.getElementById('lnameError').innerHTML="Last Name is required";
        document.getElementById("lnameError").style.color ="red";
		status = false;
		}
        
		else if(lName.length <=2 || lName.length>50)
		{
		document.getElementById('lnameError').innerHTML="Only 50 characters are allowed";
        document.getElementById("lnameError").style.color ="red";
		status = false;
		}
        
		else if(!isNaN(lName))
		{
		document.getElementById('lnameError').innerHTML="Only Alphabets are allowed";
        document.getElementById("lnameError").style.color ="red";
		status = false;
		}

		if(emailid == "")
		{
		document.getElementById('emailError').innerHTML="Email Id is required";
        document.getElementById('emailError').style.color ="red";
		status = false;
		}
        
		else if(emailid.indexOf('@') <=0 )
		{
		document.getElementById('emailError').innerHTML="Incorrect Foramt of Email";
        document.getElementById('emailError').style.color ="red";
		status = false;
		}
        
		else if((emailid.charAt(emailid.length - 4)!='.') && (emailid.charAt(emailid.length - 3)!='.'))
		{
		document.getElementById('emailError').innerHTML="Invalid Email";
        document.getElementById('emailError').style.color ="red";
		status= false;
		}
		
		if(phoneNo == "")
		{	
		document.getElementById('phoneError').innerHTML="Phone No is required";
        document.getElementById("phoneError").style.color ="red";
		status = false;
		}
        
		else if(isNaN(phoneNo))
		{	
		document.getElementById('phoneError').innerHTML="Only digits are required";
        document.getElementById("phoneError").style.color ="red";
		status = false;
		}
        
		else if(phoneNo.length !=10)
		{	
		document.getElementById('phoneError').innerHTML="Mobile No must be 10 digits only";
        document.getElementById("phoneError").style.color ="red";
		status = false;
		}	
        
		
		if(address == "")
		{
		document.getElementById('addressError').innerHTML="Address is required";
        document.getElementById("addressError").style.color ="red";
		status = false;
		}
        
      return status;
        
    }
   
    function validationForTable(){
        var isValid = validation();
        var formData = readFormData;
        if(isValid){
            if(selectedRow==null){
              AddRow();   
            }else{
            updateRow(formData);
               
            }
        }
        
    }

